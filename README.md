# Dark Sky Meter Firmware
This repository contains the firmware source code for the Dark Sky Meter, an Open Hardware low-cost night sky sensor that measures light levels as well as several ambient parameters.

## Features
* Measures light intensity ([TSL2591](https://ams.com/documents/20143/9331680/TSL2591_DS000338_7-00.pdf))
* Measures _ambient_ (inside the enclosure) temperature, humidity and pressure ([BME280](https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bme280-ds002.pdf))
* Measures position ([ADXL345](https://www.analog.com/media/en/technical-documentation/data-sheets/ADXL345-EP.pdf))
* Powered by solar panels and batteries
* WiFi connectivity
* User configurator available through WiFi AP
* 1x user LED and 1x user button

## Current state
We are in an initial state of development: Setting up the requirements, documentation and hardware/software repositories.

## Development
### Building from sources
```shell
rustup toolchain install nightly-2023-08-03 --component rust-src
cargo install cargo-espflash espflash ldproxy

# NOTE the --release tag. Debug version doesn't currently fit the partition.
cargo run --release
cargo build --release
```

Please read the [wiki page](https://gitlab.com/scrobotics/optical-makerspace/dark-sky-meter-fw/-/wikis/development/development) for more details.

## Contributing
TBD

## License
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://gitlab.com/scrobotics/optical-makerspace/dark-sky-meter-fw/-/blob/master/COPYING)

This tool is released under the MIT license, hence allowing commercial use of the library. Please refer to the [COPYING](https://gitlab.com/scrobotics/optical-makerspace/dark-sky-meter-fw/-/blob/master/COPYING) file.
