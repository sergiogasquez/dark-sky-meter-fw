// Store a value in the RTC Slow memory.
//
// https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/memory-types.html#rtc-slow-memory
// https://www.espressif.com/sites/default/files/documentation/esp32_technical_reference_manual_en.pdf#subsubsection.31.3.6

use esp_idf_sys as _; // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported

use esp_idf_svc::log::EspLogger;
use log::*;

#[no_mangle]
#[link_section = ".rtc_noinit"]
pub static mut VARIABLE_NOINIT: u32 = 1; // this value is not used, as it is never init

pub static mut VARIABLE_INIT: u32 = 1;

fn main() {
    // Temporary. Will disappear once ESP-IDF 4.4 is released, but for now it is necessary to call this function once,
    // or else some patches to the runtime implemented by esp-idf-sys might not link properly.
    esp_idf_sys::link_patches();
    EspLogger::initialize_default();

    {
        let mut variable_noinit_read;
        unsafe {
            variable_noinit_read = VARIABLE_NOINIT;
        }

        info!("Current rtc_noinit variable value is {variable_noinit_read} (should change)");

        variable_noinit_read += 1;

        if variable_noinit_read > 100 {
            variable_noinit_read = 0;
        }

        unsafe {
            VARIABLE_NOINIT = variable_noinit_read;
        }
    }

    {
        let mut variable_init_read;
        unsafe {
            variable_init_read = VARIABLE_INIT;
        }

        info!("Current init variable value is {variable_init_read} (should never change)");

        variable_init_read += 1;

        if variable_init_read > 100 {
            variable_init_read = 0;
        }

        unsafe {
            VARIABLE_INIT = variable_init_read;
        }
    }

    let sleep_micros = 2_000_000;
    unsafe {
        esp_idf_sys::esp_sleep_enable_timer_wakeup(sleep_micros);

        info!("Going to deep sleep {} seconds", sleep_micros / 1_000_000);
        esp_idf_sys::esp_deep_sleep_start();
        // Software reset!
    }
}
